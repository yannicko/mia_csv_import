<?php

namespace Drupal\mia_csv_import\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\file\Entity\File;
use Drush\Commands\DrushCommands;
use Drupal\Core\Config\ConfigFactory;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class BatchDrushCommands extends DrushCommands {
  const SETTINGS = 'mia_csv_import.settings';


  private $nodeTypes;
  protected $configFactory;

  /**
   * Entity type service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;
  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;
  /**
   * Constructs a new BatchDrushCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigFactory $configFactory, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
    $this->loggerChannelFactory = $loggerChannelFactory;
  }
  /**
   * Update Node.
   *
   * @param string $type
   *   Type of node to update
   *   Argument provided to the drush command.
   *
   * @command mia_csv_import:nodes
   * @aliases mcin
   *
   * @usage mia_csv_import:nodes node_type
   *   node_type is the type of node to update
   */
  public function importNodes($type = '') {
    $config = $this->configFactory->get(self::SETTINGS);

    if (empty($type)) {
      $this->nodeTypes = $this->entityTypeManager->getStorage('node_type')
        ->loadMultiple();
    }
    else {
      try {
        $this->nodeTypes[] = $this->entityTypeManager->getStorage('node_type')
          ->loadByProperties(['name' => $type]);
      } catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('mia_csv_import')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    foreach ($this->nodeTypes as $nodeType) {
      $delimiter = $config->get($nodeType->id() . '_delimiter');
      $csvFile = $config->get($nodeType->id() . '_import_csv');
      if (!empty($csvFile)) {
        $fieldMapping = $config->get('field_mappings');
        $fieldMapping = $fieldMapping[$nodeType->id()];
        $uniqueIdField = $fieldMapping['unique_id_field'];
        $file = File::load($csvFile[0]);
        $data = $this->csvtoarray($file->getFileUri(), $delimiter);
        $queue = \Drupal::queue($nodeType->id() . '_import_node_queue');
        $queue->createQueue();
        $collectedData = [];
        foreach ($data as $row) {
          $collectedData['field_mappings'] = $fieldMapping;
          $collectedData['node_type'] = $nodeType->id();
          $collectedData['unique_id_field'] = $uniqueIdField;
          $collectedData['row'] = $row;
          $queue->createItem($collectedData);
        }
      }
    }

  }

  public function csvtoarray($filename, $delimiter) {

    if (!file_exists($filename) || !is_readable($filename)) return FALSE;
    $data = [];
    $header = NULL;

    if (($handle = fopen($filename, 'r')) !== FALSE) {
      while (($row = fgetcsv($handle, '1000', $delimiter)) !== FALSE) {
        if (!$header) {
          $header = $row;
        }
        else {
          $data[] = array_combine($header, $row);
        }
      }
      fclose($handle);
    }

    return $data;
  }
}