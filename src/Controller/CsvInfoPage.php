<?php

namespace Drupal\mia_csv_import\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

Class CsvInfoPage extends ControllerBase {

  public function content(Request $request) {

    $form = \Drupal::formBuilder()->getForm('Drupal\mia_csv_import\Form\CsvInfoForm');

    return $form;

  }

}