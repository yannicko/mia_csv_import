<?php

namespace Drupal\mia_csv_import\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\node\Entity\Node;

class CreateNodeEvent extends Event {

  const EVENT_NAME = 'mia_csv_import.create_node';

  private $item;
  private $fieldMappings;
  private $node;
  private $action;

  public function __construct($item, $field_mappings, Node $node, $action) {
    $this->item = $item;
    $this->fieldMappings = $field_mappings;
    $this->node = $node;
    $this->action = $action;
  }

  public function getItem() {
    return $this->item;
  }

  public function getFieldMappings() {
    return $this->fieldMappings;
  }

  public function getNode() {
    return $this->node;
  }

  public function getAction() {
    return $this->action;
  }
}