<?php

namespace Drupal\mia_csv_import\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CsvInfoForm extends ConfigFormBase {

  const SETTINGS = 'mia_csv_import.settings';

  protected $nodeStorage;

  public function __construct(EntityStorageInterface $node_storage) {
    $this->nodeStorage = $node_storage;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('node_type')
    );
  }

  public function getFormId() {
    return 'mia_csv_info_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::SETTINGS);

    $node_types = $this->nodeStorage->loadMultiple();

    $form['csv'] = [
      '#type' => 'container',
    ];

    $form['csv']['description'] = [
      '#markup' => t('<p>Use this form to upload a CSV file, or enter a remote CSV file.<br />If a file is uploaded, it will have priority on the import.</p>')
    ];

    foreach ($node_types as $node_type) {
      $form['csv'][$node_type->id()] = [
        '#type' => 'details',
        '#title' => $node_type->label(),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      ];

      $form['csv'][$node_type->id()][$node_type->id() . '_delimiter'] = [
        '#type' => 'select',
        '#title' => $this->t('Delimiter'),
        '#default_value' => $config->get($node_type->id() . '_delimiter'),
        '#options' => [
          ',' => ',',
          ';' => ';',
          't' => 'tab',
          '|' => '|',
        ]
      ];

      $form['csv'][$node_type->id()][$node_type->id() . '_remote_csv'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Remote CSV file'),
        '#default_value' => $config->get($node_type->id() . '_remote_csv'),
      ];

      $form['csv'][$node_type->id()][$node_type->id() . '_import_csv'] = [
        '#type' => 'managed_file',
        '#title' => $this->t('Upload CSV file'),
        '#upload_location' => 'public://importcsv/',
        '#default_value' => $config->get($node_type->id() . '_import_csv'),
        '#upload_validators' => ['file_validate_extensions' => ['csv']],
      ];
    }

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Upload CSV'),
      '#button_type' => 'primary',
    ];

    return $form;

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $node_types = $this->nodeStorage->loadMultiple();

    foreach ($node_types as $node_type_name => $node_type) {
      $this->configFactory->getEditable(static::SETTINGS)
        ->set($node_type_name . '_delimiter', $form_state->getValue($node_type_name . '_delimiter'))
        ->set($node_type_name . '_remote_csv', $form_state->getValue($node_type_name . '_remote_csv'))
        ->set($node_type_name . '_import_csv', $form_state->getValue($node_type_name . '_import_csv'))
        ->save();

      if (!empty($form_state->getValue($node_type_name . '_import_csv'))) {
        $csv_file = $form_state->getValue($node_type_name . '_import_csv');
        $file = File::load($csv_file[0]);
        $file->setPermanent();
        $file->save();
      }
    }
    parent::submitForm($form, $form_state);

  }

}