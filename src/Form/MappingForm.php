<?php

namespace Drupal\mia_csv_import\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MappingForm extends ConfigFormBase {

  const SETTINGS = 'mia_csv_import.settings';

  protected $nodeStorage;

  public function __construct(EntityStorageInterface $node_storage) {
    $this->nodeStorage = $node_storage;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('node_type')
    );
  }

  public function getFormId() {
    return 'mia_csv_import_mapping_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['description'] = [
      '#markup' => $this->t('<p>Use this form to set field mappings.</p>'),
    ];

    $form['#tree'] = TRUE;

    $node_types = $this->nodeStorage->loadMultiple();

    $form['field_mappings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field mappings'),
    ];

    foreach ($node_types as $node_type) {
      $field_mappings = $config->get('field_mappings');
      $entityManager = \Drupal::service('entity_field.manager');
      $fields = $entityManager->getFieldDefinitions('node', $node_type->id());

      $options = [];
      foreach ($fields as $field_name => $field) {
        $options[$field_name] = $field_name;
      }

      $form['field_mappings'][$node_type->id()] = [
        '#type' => 'details',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => $node_type->label(),
      ];

      $form['field_mappings'][$node_type->id()]['unique_id_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Unique ID field'),
        '#description' => $this->t('Select a field which serves as purpose to match the content in the website with the content in the CSV, based on ID'),
        '#default_value' => $field_mappings[$node_type->id()]['unique_id_field'],
        '#options' => $options,
      ];

      $form['field_mappings'][$node_type->id()]['fields'] = [
        '#type' => 'table',
        '#header' => array('Drupal field', 'CSV field', 'Multiple values?'),
      ];

      foreach ($fields as $key => $field) {
        if (!$field->isReadOnly()) {
          $form['field_mappings'][$node_type->id()]['fields'][$key] = [
          ];

          $form['field_mappings'][$node_type->id()]['fields'][$key]['drupal_field'] = [
            '#type' => 'textfield',
            '#default_value' => $field->getName(),
          ];

          $form['field_mappings'][$node_type->id()]['fields'][$key]['mapped_field'] = [
            '#type' => 'textfield',
            '#default_value' => isset($field_mappings[$node_type->id()]['fields'][$key]['mapped_field']) ? $field_mappings[$node_type->id()]['fields'][$key]['mapped_field'] : '',
          ];

          $form['field_mappings'][$node_type->id()]['fields'][$key]['is_multivalue'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Multiple values'),
            '#default_value' => isset($field_mappings[$node_type->id()]['fields'][$key]['is_multivalue']) ? $field_mappings[$node_type->id()]['fields'][$key]['is_multivalue'] : '',
          ];
        }
      }
    }

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('field_mappings', $form_state->getValue('field_mappings'))
      ->save();

    parent::submitForm($form, $form_state);

  }



}