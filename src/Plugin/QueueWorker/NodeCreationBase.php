<?php

namespace Drupal\mia_csv_import\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\mia_csv_import\Event\CreateNodeEvent;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class NodeCreationBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  protected $nodeStorage;
  protected $termStorage;
  protected $fieldMappings;
  protected $item;

  public function __construct(EntityStorageInterface $node_storage, EntityStorageInterface $taxonomy_term_storage) {
    $this->nodeStorage = $node_storage;
    $this->termStorage = $taxonomy_term_storage;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity.manager')->getStorage('node'),
      $container->get('entity.manager')->getStorage('taxonomy_term')
    );
  }

  protected function setNodeFieldValues(Node $node, $item, $fieldMappings, $action) {
    foreach ($fieldMappings['fields'] as $field => $field_mapping) {
      if ($field_mapping['is_multivalue']) {
        $item[$field_mapping['mapped_field']] = explode(',', $item[$field_mapping['mapped_field']]);
        foreach ($item[$field_mapping['mapped_field']] as $key => $term) {
          $term_name = $term;
          $term = $this->termExists($term_name, 'tags');
          $item[$field_mapping['mapped_field']][$key] = ['target_id' => $term->tid->value];
        }
      }
      if (!empty($field_mapping['mapped_field'])) {
        $node->set($field, $item[$field_mapping['mapped_field']]);
      }
    }
  }

  protected function saveNode($item, $fieldMappings, Node $node, $action) {
    $event = new CreateNodeEvent($item, $fieldMappings, $node, $action);
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event_dispatcher->dispatch(CreateNodeEvent::EVENT_NAME, $event);

    $node->save();
  }

  public function processItem($data) {
    $item = $data['row'];
    $node_type_name = $data['node_type'];
    $fieldMappings = $data['field_mappings'];
    $unique_id_field = $data['unique_id_field'];

    $nodes = $this->nodeStorage->loadByProperties([
      $unique_id_field => $item[$fieldMappings['fields'][$unique_id_field]['mapped_field']],
      'type' => $node_type_name,
    ]);

    if ($node = reset($nodes)) {
      $this->setNodeFieldValues($node, $item, $fieldMappings, 'update');
    }
    else {
      $node = Node::create(['type' => $node_type_name]);
      $node->set('uid', 1);
      $node->enforceIsNew();
      $this->setNodeFieldValues($node, $item, $fieldMappings, 'create');
    }
  }

  protected function termExists($term_name, $vocabulary, $extras = []) {
    $term = $this->termStorage->loadByProperties([
      'name' => $term_name
    ]);

    if (!empty($term)) {
      return reset($term);
    }
    else {
      $arguments = [
        'name' => $term_name,
        'vid' => $vocabulary,
      ];

      if (!empty($extras)) {
        $arguments = array_merge($arguments, $extras);
      }

      $term = Term::create($arguments);
      $term->save();
      return $term;
    }
  }

  protected function nodeExists($node_title, $node_type, $fields = []) {
    $arguments = [
      'title' => $node_title,
      'type' => $node_type,
    ];

    if (!empty($fields)) {
      array_merge($arguments, $fields);
    }

    $node = $this->nodeStorage->loadByProperties($arguments);

    if ($node = reset($node)) {
      return $node;
    }
    else {
      $node = Node::create(['type' => $node_type]);
      $node->set('title', $node_title);
      $node->set('uid', 1);
      $node->set('field_specialism_type', 'specialism');
      $node->enforceIsNew();
      return $node;
    }
  }


}