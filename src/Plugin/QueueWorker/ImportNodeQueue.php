<?php
/*
 * @file
 * Contains Drupal\mia_csv_import\Plugin\QueueWorker\ImportNodeQueue.
 */

namespace Drupal\mia_csv_import\Plugin\QueueWorker;


/**
 * Processes items from CSV to nodes
 *
 * @QueueWorker(
 *   id = "import_node_queue",
 *   title = @Translation("Import Node Queue"),
 *   cron = {"time" = 10}
 * )
 */
class ImportNodeQueue extends NodeCreationBase {}