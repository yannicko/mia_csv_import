<?php
/*
 * @file
 * Contains Drupal\mia_csv_import\Plugin\QueueWorker\ManualImportNodeQueue.
 */

namespace Drupal\mia_csv_import\Plugin\QueueWorker;


use Drupal\node\Entity\Node;

/**
 * Processes items from CSV to nodes
 *
 * @QueueWorker(
 *   id = "manual_import_node_queue",
 *   title = @Translation("Manual Import Node Queue"),
 * )
 */
class ManualImportNodeQueue extends NodeCreationBase {

  protected function setNodeFieldValues(Node $node, $item, $fieldMappings, $action) {
  }
}